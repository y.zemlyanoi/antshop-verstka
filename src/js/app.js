const initRangeSlider = () => {
  var sheet = document.createElement('style'),
      $rangeInput = $('.range input'),
      prefs = ['-webkit-slider-thumb', '-moz-range-thumb', '-ms-thumb', 'webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];

  document.body.appendChild(sheet);

  var getTrackStyle = function (el) {
    var curVal = el.value, style = '';

    // Set active label
    $('.range-labels li').removeClass('active selected');

    var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');

    curLabel.addClass('active selected');
    curLabel.prevAll().addClass('selected');

    // Change background gradient
    for (var i = 0; i < prefs.length; i++) {
      if($(el).val() === '10')
        style += '.range input::-' + prefs[i] + '{margin-left:-10px;margin-right:0px;}';
      else if($(el).val() === '30')
        style += '.range input::-' + prefs[i] + '{margin-right:-13px;margin-left:0px}';
      else
        style += '.range input::-' + prefs[i] + '{margin-left:0px;margin-right:0px;}';
    }
    return style;
  };

  $rangeInput.on('input', function () {
    sheet.textContent = getTrackStyle(this);
  });

// Change input value on label click
  $('.range-labels li').on('click', function () {
    var index = $(this).index();

    $rangeInput.val(5*index + 10).trigger('input');

  });
};

$(document).ready(()=> {
  initRangeSlider();
});